﻿using System;
namespace Trackfiend.Artist.Models.ConfigurationModels
{
    public class SolrOptions
    {
        public const string SolrConfiguration = "SolrConfiguration";

        public string HostUri { get; set; }

        public string ArtistUri { get; set; }
    }
}
