﻿using System;
using MassTransit.RabbitMqTransport;

namespace Trackfiend.Artist.Services.Models
{
    public class QueueOptions
    {
        public static string QueueSettings = "QueueConfiguration";

        public string Username { get; set; }

        public string Password { get; set; }

        public string HostName { get; set; }

        public string VirtualHost { get; set; }

        public string ArtistExchangeName { get; set; }

        public string ArtistQueueName { get; set; }
    }
}
