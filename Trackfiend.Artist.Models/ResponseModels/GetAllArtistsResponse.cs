﻿using System;
using System.Collections.Generic;

namespace Trackfiend.Artist.Models.ResponseModels
{
    public class GetAllArtistsResponse
    {
        public List<ArtistModel> Artists { get; set; }

        public long Total { get; set; }

        public override string ToString()
        {
            return string.Join<ArtistModel>(",", Artists);
        }
    }
}
