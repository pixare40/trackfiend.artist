﻿using System;
namespace Trackfiend.Artist.Services.Models
{
    public class ArtistUpdateModel
    {
        public static string ArtistUpdate = "ArtistUpdate";

        public string UpdateFilePath { get; set; }
    }
}
