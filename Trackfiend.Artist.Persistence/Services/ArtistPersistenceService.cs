﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.Extensions.Options;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using Trackfiend.Artist.Models;
using Trackfiend.Artist.Services.ConfigurationModels;
using Trackfiend.Artist.Models.ResponseModels;

namespace Trackfiend.Artist.Persistence.Services
{
    public class ArtistPersistenceService
    {
        private readonly IMongoCollection<ArtistModel> artistCollection;

        public ArtistPersistenceService(IOptions<DatabaseConfigurationModel> configuration)
        {
            MongoClient client = new MongoClient(configuration.Value.ConnectionString);
            IMongoDatabase database = client.GetDatabase(configuration.Value.DatabaseName);
            artistCollection = database.GetCollection<ArtistModel>(configuration.Value.CollectionName);
        }

        public MBArtistModel ProcessArtist(string artist)
        {
            MBArtistModel artistModel = BsonSerializer.Deserialize<MBArtistModel>(artist);

            try
            {
                BsonDocument document = BsonSerializer.Deserialize<BsonDocument>(artist);
                var findDocDef = Builders<ArtistModel>.Filter.Eq(x => x.Id, artistModel.Id);
                //var updateDefinition = Builders<ArtistModel>.Update.

                var foundartist = artistCollection.Find(findDocDef);

                if (foundartist.CountDocuments() > 0)
                {
                    Console.WriteLine("We found {0} artists.", foundartist.CountDocuments());
                    Console.WriteLine("We found artist {0}", foundartist.FirstOrDefault());
                    Task<ReplaceOneResult> replacedArtist = artistCollection.ReplaceOneAsync(findDocDef, artistModel);
                    Console.WriteLine("document replaced");
                    return artistModel;
                }

                artistCollection.InsertOne(artistModel);

                Console.WriteLine("Artist inserted");
                Console.WriteLine(artistModel);
                return artistModel;
            }
            catch(Exception e)
            {
                Console.WriteLine(e);
                Console.WriteLine("Failed to store {0}", artistModel.Name);
                return null;
            }
        }

        public async Task<ArtistModel> GetArtistById(string id)
        {
            var filter = Builders<ArtistModel>.Filter.Eq(x => x.Id, id);
            var result = await artistCollection.FindAsync(filter);

            return result.FirstOrDefault();
        }

        public List<ArtistModel> GetArtistsByName(string name)
        {
            FilterDefinition<ArtistModel> filter = Builders<ArtistModel>.Filter.Eq(x => x.Name, name);
            List<ArtistModel> artist = artistCollection.Find(filter).ToList();

            return artist;
        }

        public async Task<GetAllArtistsResponse> GetPagedArtists(int page, int pageSize)
        {
            page = page == 0 ? 1 : page;

            var allArtistsFilter = Builders<ArtistModel>.Filter.Empty;
            List<ArtistModel> data = await artistCollection.Find(allArtistsFilter)
                .Skip((page - 1) * pageSize)
                .Limit(pageSize)
                .ToListAsync();
            long totalNumber = await artistCollection.CountDocumentsAsync(allArtistsFilter);

            return GetArtistsResponse(data, totalNumber);
        }

        [Obsolete("Use Solr search with the search service")]
        public async Task<GetAllArtistsResponse> SearchArtist(string searchTerm, int page)
        {
            page = page == 0 ? 1 : page;

            if (ShouldCreateIndex())
            {
                CreateIndexOptions indexOptions = new CreateIndexOptions { Name = "ArtistNameSearchIndex" };
                var indexKeys = Builders<ArtistModel>.IndexKeys.Text(artist => artist.Name);
                var indexModel = new CreateIndexModel<ArtistModel>(indexKeys, indexOptions);
                await artistCollection.Indexes.CreateOneAsync(indexModel);
            }

            var searchArtistFilter = Builders<ArtistModel>.Filter
                .Text(searchTerm, new TextSearchOptions { CaseSensitive = false });

            List<ArtistModel> data = await artistCollection.Find(searchArtistFilter)
                .Skip((page - 1) * 10)
                .Limit(10)
                .ToListAsync();
            long totalNumber = await artistCollection.CountDocumentsAsync(searchArtistFilter);

            return GetArtistsResponse(data, totalNumber);
        }

        public async Task<ArtistModel> CreateArtist(ArtistModel artist)
        {
            artist.Id = Guid.NewGuid().ToString();
            await artistCollection.InsertOneAsync(artist);

            return artist;
        }

        public async Task<ArtistModel> UpdateArtist(ArtistModel artist)
        {
            throw new NotImplementedException();
        }

        private GetAllArtistsResponse GetArtistsResponse(List<ArtistModel> artists, long total)
        {
            return new GetAllArtistsResponse { Artists = artists, Total = total };
        }

        private bool ShouldCreateIndex()
        {
            var indexes = artistCollection.Indexes.List().ToList();

            foreach (var index in indexes)
            {
                if(index.GetElement("name").Value.AsString == "ArtistNameSearchIndex"){
                    return false;
                }
            }

            return true;
        }
    }
}
