using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Trackfiend.Artist.Models;
using Trackfiend.Artist.Models.ConfigurationModels;
using Trackfiend.Artist.Persistence.Services;
using Trackfiend.Artist.Services.ConfigurationModels;
using SolrNet;
using Trackfiend.Artist.Search;

namespace Trackfiend.Artist
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();
            services.AddControllers();
            services.Configure<DatabaseConfigurationModel>(Configuration
                        .GetSection(DatabaseConfigurationModel.DatabaseConfiguration));
            services.AddSingleton<ArtistPersistenceService>();

            SolrOptions solrOptions = Configuration
                    .GetSection(SolrOptions.SolrConfiguration).Get<SolrOptions>();

            SolrNet.Startup.Init<ArtistSearchModel>(solrOptions.ArtistUri);
            services.AddSingleton<SearchService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            if (env.IsProduction())
            {
                app.UseHttpsRedirection();
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
