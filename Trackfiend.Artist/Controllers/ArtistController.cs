﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SolrNet;
using Trackfiend.Artist.Models;
using Trackfiend.Artist.Models.ResponseModels;
using Trackfiend.Artist.Persistence.Services;
using Trackfiend.Artist.Search;

namespace Trackfiend.Artist.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ArtistController : ControllerBase
    {
        private readonly ArtistPersistenceService artistPersistenceService;
        private readonly SearchService searchService;

        public ArtistController(ArtistPersistenceService artistPersistenceService,
            SearchService searchService)
        {
            this.artistPersistenceService = artistPersistenceService;
            this.searchService = searchService;
        }

        [HttpGet("All/{page:int}")]
        public async Task<ActionResult<GetAllArtistsResponse>> AllAsync(int page)
        {
            if(page < 1)
            {
                page = 1;
            }

            int take = 50;
            GetAllArtistsResponse artistsResponse = await artistPersistenceService.GetPagedArtists(page, take);

            return Ok(artistsResponse);
        }

        [HttpGet("GetArtist/{id}")]
        public async Task<ActionResult<ArtistModel>> GetArtist(string id)
        {
            ArtistModel artist = await artistPersistenceService.GetArtistById(id);

            return Ok(artist);
        }

        [HttpGet("Search")]
        public async Task<ActionResult<SolrQueryResults<ArtistSearchModel>>> SearchAsync([FromQuery(Name = "searchTerm")] string searchTerm,
            [FromQuery(Name = "page")] int page)
        {
            searchTerm = searchTerm == null ? "*" : searchTerm;

            SolrQueryResults<ArtistSearchModel> searchResults = await searchService.SearchArtists(searchTerm,
                20, page);

            return Ok(searchResults);
        }


        [HttpPost("Create")]
        public async Task<ActionResult<ArtistModel>> CreateArtistAsync(ArtistModel artist)
        {
            ArtistModel data = await artistPersistenceService.CreateArtist(artist);

            return Ok(data);
        }

        [HttpPost("Update")]
        public async Task<ActionResult<ArtistModel>> UpdateArtistAsync(ArtistModel artist)
        {
            ArtistModel data = await artistPersistenceService.UpdateArtist(artist);

            return Ok(data);
        }
    }
}
