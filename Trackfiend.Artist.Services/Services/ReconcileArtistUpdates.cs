﻿using System;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Options;
using Trackfiend.Artist.Models;
using Trackfiend.Artist.Services.Models;

namespace Trackfiend.Artist.Services.Services
{
    public class ReconcileArtistUpdates
    {
        private readonly IBusControl busControl;

        public ReconcileArtistUpdates(IBusControl busControl)
        {
            this.busControl = busControl;
        }

        public Task ReportAddition(MBArtistModel artist)
        {
            return Task.Run(async ()=>
            {
                ISendEndpoint sendEndpoint = await busControl.GetSendEndpoint(new Uri("queue:artist.updates"));
                await sendEndpoint.Send(artist, e=>{
                    e.MessageId = Guid.NewGuid();
                });
            });
        }
    }
}
