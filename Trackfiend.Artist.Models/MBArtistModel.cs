﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace Trackfiend.Artist.Models
{
    public class MBArtistModel : ArtistModel
    {
        [BsonElement("ipis")]
        public object Ipis { get; set; }

        [BsonElement("tags")]
        public object Tags { get; set; }

        [BsonElement("isnis")]
        public object Isnis { get; set; }

        [BsonElement("rating")]
        public object Rating { get; set; }

        [BsonElement("aliases")]
        public object Aliases { get; set; }

        [BsonElement("country")]
        public string Country { get; set; }

        [BsonElement("type-id")]
        public string TypeId { get; set; }

        [BsonElement("end_area")]
        public object EndArea { get; set; }

        [BsonElement("end-area")]
        public object EndArea2 { get; set; }

        [BsonElement("gender-id")]
        public string GenderId { get; set; }

        [BsonElement("life-span")]
        public object LifeSpan { get; set; }

        [BsonElement("relations")]
        public object Relations { get; set; }

        [BsonElement("sort-name")]
        public string SortName { get; set; }

        [BsonElement("annotation")]
        public object Annotation { get; set; }

        [BsonElement("begin_area")]
        public object BeginArea { get; set; }

        [BsonElement("begin-area")]
        public object BeginArea2 { get; set; }

        [BsonElement("disambiguation")]
        public string Disambiguation { get; set; }

        [BsonElement("genres")]
        public object Genres { get; set; }
    }
}
