﻿using System;
namespace Trackfiend.Artist.Services.Models
{
    public class RunUpdateOptions
    {
        public static string RunUpdateValueObject = "RunUpdateService";

        public bool RunUpdates { get; set; }
    }
}
