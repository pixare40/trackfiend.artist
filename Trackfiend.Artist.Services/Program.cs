using MassTransit;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RabbitMQ.Client;
using Trackfiend.Artist.Persistence.Services;
using Trackfiend.Artist.Services.ConfigurationModels;
using Trackfiend.Artist.Services.Models;
using Trackfiend.Artist.Services.Services;
using SolrNet;
using Trackfiend.Artist.Models;
using Trackfiend.Artist.Search;
using Trackfiend.Artist.Models.ConfigurationModels;

namespace Trackfiend.Artist.Services
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.Configure<DatabaseConfigurationModel>(hostContext
                        .Configuration
                        .GetSection(DatabaseConfigurationModel.DatabaseConfiguration));

                    services.Configure<RunUpdateOptions>(hostContext.Configuration
                        .GetSection(RunUpdateOptions.RunUpdateValueObject));

                    services.Configure<QueueOptions>(hostContext.Configuration
                        .GetSection(QueueOptions.QueueSettings));

                    services.Configure<ArtistUpdateModel>(hostContext.Configuration
                        .GetSection(ArtistUpdateModel.ArtistUpdate));

                    services.AddSingleton<ArtistPersistenceChannelService>();
                    services.AddSingleton<ArtistPersistenceService>();
                    services.AddHostedService<UpdateArtistInformationService>();
                    services.AddMassTransit(x =>
                    {
                        x.AddBus(serviceProvider =>
                        {
                            return Bus.Factory.CreateUsingRabbitMq(config =>
                            {
                                QueueOptions queueOptions = hostContext
                                .Configuration
                                .GetSection(QueueOptions.QueueSettings).Get<QueueOptions>();

                                config.Host(queueOptions.HostName, queueOptions.VirtualHost, h =>
                                {
                                    h.Username(queueOptions.Username);
                                    h.Password(queueOptions.Password);
                                });

                                config.ExchangeType = ExchangeType.Fanout;
                                config.AutoDelete = false;
                                config.Durable = true;
                            });
                        });
                        
                    });

                    services.AddSingleton<ReconcileArtistUpdates>();
                    SolrOptions solrOptions = hostContext
                    .Configuration
                    .GetSection(SolrOptions.SolrConfiguration).Get<SolrOptions>();

                    Startup.Init<ArtistSearchModel>(solrOptions.ArtistUri);
                    services.AddSingleton<SearchService>();
                });
    }
}
