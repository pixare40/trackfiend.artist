﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace Trackfiend.Artist.Models
{
    public class ArtistModel
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public ObjectId DocumentId { get; set; }

        [BsonElement("id")]
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        [BsonElement("name")]
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        [BsonElement("realname")]
        [JsonProperty(PropertyName = "realname")]
        public string RealName { get; set; }

        [BsonElement("gender")]
        [JsonProperty(PropertyName = "gender")]
        public string Gender { get; set; }

        [BsonElement("type")]
        [JsonProperty(PropertyName = "type")]
        public string Type { get; set; }

        [BsonElement("area")]
        [JsonProperty(PropertyName = "area")]
        public object Area { get; set; }

        [BsonElement("existence_start")]
        [JsonProperty(PropertyName = "existence_start")]
        public DateTime Start { get; set; }

        [BsonElement("existence_end")]
        [JsonProperty(PropertyName = "existence_end")]
        public DateTime End { get; set; }

        [BsonElement("mbid")]
        [JsonProperty(PropertyName = "mbid")]
        public string Mbid { get; set; }

        [BsonElement("cname")]
        [JsonProperty(PropertyName = "cname")]
        public string Cname { get; set; }

        [BsonElement("discogsid")]
        [JsonProperty(PropertyName = "discogsid")]
        public string DiscogsId { get; set; }

        [BsonElement("biography")]
        [JsonProperty(PropertyName = "biography")]
        public string Biography { get; set; }

        [BsonElement("comments")]
        [JsonProperty(PropertyName = "comments")]
        public string Comments { get; set; }

        [BsonElement("artist_picture")]
        [JsonProperty(PropertyName = "artist_picture")]
        public string ArtistPicture { get; set; }

        public override string ToString()
        {
            return string.Format("{0}", Name);
        }
    }
}
