﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using CommonServiceLocator;
using SolrNet;
using SolrNet.Commands.Parameters;
using Trackfiend.Artist.Models;

namespace Trackfiend.Artist.Search
{
    public class SearchService
    {
        private readonly ISolrOperations<ArtistSearchModel> solrOperations;

        public SearchService()
        {
            solrOperations = ServiceLocator
                .Current.GetInstance<ISolrOperations<ArtistSearchModel>>();
        }

        public void AddArtistToSearchIndex(MBArtistModel artist)
        {
            ArtistSearchModel searchModel =
                new ArtistSearchModel(artist.Id, artist.Name);

            var result = solrOperations.Add(searchModel);

            if (result.Status != 0)
            {
                Console.WriteLine("Adding index failed, please check code");
                return;
            }

            solrOperations.Commit();
        }

        public async Task<SolrQueryResults<ArtistSearchModel>> SearchArtists(string artistName,
            int pageSize,
            int page)
        {
            int offset = (page - 1) * pageSize;

            QueryOptions queryOptions = new QueryOptions
            {
                Rows = pageSize,
                StartOrCursor = new StartOrCursor.Start(offset),
            };

            SolrQuery query = new SolrQuery(string.Format("name:{0}", artistName));
            SolrQueryResults<ArtistSearchModel> solrQueryResults =
                await solrOperations.QueryAsync(query, queryOptions);

            return solrQueryResults;
        }
    }
}
