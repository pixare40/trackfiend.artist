﻿using System;
using System.Threading.Channels;
using Trackfiend.Artist.Models;

namespace Trackfiend.Artist.Services.Services
{
    public class ArtistPersistenceChannelService
    {
        public Channel<string> ArtistChannel { get; set; }

        public ArtistPersistenceChannelService()
        {
            ArtistChannel = Channel.CreateBounded<string>(200);
        }
    }
}
