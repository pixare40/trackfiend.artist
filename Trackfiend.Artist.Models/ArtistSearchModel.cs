﻿using System;
using SolrNet.Attributes;
using System.Collections.Generic;

namespace Trackfiend.Artist.Models
{
    public class ArtistSearchModel
    {
        public ArtistSearchModel() { }

        public ArtistSearchModel(string id, string name)
        {
            Id = id;
            Name = new string[] { name };
        }

        [SolrUniqueKey("id")]
        public string Id { get; set; }

        [SolrField("name")]
        public ICollection<string> Name { get; set; }
    }
}
