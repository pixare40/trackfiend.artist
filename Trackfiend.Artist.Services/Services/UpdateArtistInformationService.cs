﻿using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using Trackfiend.Artist.Models;
using Trackfiend.Artist.Persistence.Services;
using Trackfiend.Artist.Search;
using Trackfiend.Artist.Services.Models;

namespace Trackfiend.Artist.Services.Services
{
    public class UpdateArtistInformationService: BackgroundService
    {
        private readonly ArtistPersistenceService artistPersistenceService;
        private readonly IOptions<RunUpdateOptions> runUpdateOptions;
        private readonly IBusControl busControl;
        private readonly ReconcileArtistUpdates reconcileArtistUpdates;
        private readonly SearchService updateSearchService;
        private readonly ArtistUpdateModel artistUpdateModel;

        public UpdateArtistInformationService(ArtistPersistenceService artistPersistenceService,
            IOptions<RunUpdateOptions> runUpdateOptions,
            IBusControl busControl,
            ReconcileArtistUpdates reconcileArtistUpdates,
            IOptions<ArtistUpdateModel> artistUpdateModel,
            SearchService updateSearchService)
        {
            busControl.Start();
            this.artistPersistenceService = artistPersistenceService;
            this.runUpdateOptions = runUpdateOptions;
            this.busControl = busControl;
            this.reconcileArtistUpdates = reconcileArtistUpdates;
            this.updateSearchService = updateSearchService;
            this.artistUpdateModel = artistUpdateModel.Value;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return Task.Run(delegate
            {
                if (!runUpdateOptions.Value.RunUpdates)
                {
                    return;
                }
                try
                {
                    FileStream fileStream =
                    File.OpenRead(artistUpdateModel.UpdateFilePath);
                    StreamReader reader = new StreamReader(fileStream);
                    string ar = string.Empty;

                    while ((ar = reader.ReadLine()) != null)
                    {

                        MBArtistModel artist = artistPersistenceService.ProcessArtist(ar);
                        if(artist == null)
                        {
                            return;
                        }

                        reconcileArtistUpdates.ReportAddition(artist);
                        updateSearchService.AddArtistToSearchIndex(artist);
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    Console.WriteLine("An exception occured");
                }
            });
        }

        public override void Dispose()
        {
            busControl.StopAsync();
            base.Dispose();
        }
    }
}
